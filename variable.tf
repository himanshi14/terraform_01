variable "vpc_cidr" {
    description = "VPC CIDR of Ninja-vpc-01"
    type = string
    default = "10.0.0.0/16"
}

variable "vpc_tag" {
    description = "Tags of VPC"
    type = string
    default = "consul-vpc-01"
}

variable "pub_sub_cidr" {
    description = "Public Subnet CIDR"
    type = string
    default = "10.0.0.0/24" 
}

variable "pub_sub_tag" {
    description = "Tags of Public Subnet"
    type = string
    default = "consul-pub-sub-01"
}

variable "priv_sub_cidr" {
    description = "Private Subnet CIDR"
    type = list
    default = [ "10.0.1.0/24" , "10.0.2.0/24" , "10.0.3.0/24"]
}

variable "priv_sub_tag" {
    description = "Tags of Private Subnet"
    type = string
    default = "consul-priv-sub-0"
}

variable "internet_gateway" {
    description = "Internet gateway Tag"
    type = string 
    default = "consul-igw-01"
}

variable "nat_gateway" {
    description = "NAT Gateway"
    type = string
    default = "consul-nat-01"
}

variable "elastic" {
    default = "new-elastic-ip"
}

variable "rt_01" {
    default = "consul-route-pub-01"
}

variable "rt_02" {
    default = "consul-route-priv-01"
}

variable "bastion_name" {
    default = "consul-instance-01"
}

variable "ubuntu_ami" {
    default = "ami-0fdf70ed5c34c5f52"
}

variable "bastion_ports" {
  description = "Allowed bastion ports"
  type        = map
  default     = {
    "22"  = "182.68.113.32/32" 
}
}

variable "bastion_sg" {
    default = "bastion_sg_01"
}

variable "node_ports" {
  description = "Allowed bastion ports"
  type        = map
  default     = {
    #"22"  = aws_instance.bastion.instances.private_ip"/32" 
}
}

variable "node_sg" {
    default = "node_sg_01"
}

variable "node_name" {
    default = "consul-instance"
}

variable "key_n" {
    default = "key.pem"
}
