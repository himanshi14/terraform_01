resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
   tags = {
     Name = var.vpc_tag
   }
}

data "aws_availability_zones" "available" {}

resource "aws_subnet" "pub_sub" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.pub_sub_cidr
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    Name = var.pub_sub_tag
  } 
}
#
resource "aws_subnet" "priv_sub" {
  count = length(var.priv_sub_cidr)
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.priv_sub_cidr[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags = {
    name = "consul-priv-sub-0${count.index+1}"
  } 
}
#
##"Pub-Sub-${count.index}"
resource "aws_internet_gateway" "igw1" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = var.internet_gateway
  }
}

resource "aws_eip" "elasticip" {
  vpc = true
  tags = {
    Name = var.elastic
  }
}

resource "aws_nat_gateway" "nat1" {
  allocation_id = aws_eip.elasticip.id
  subnet_id = aws_subnet.pub_sub.id
  tags = {
    Name = var.nat_gateway
  }
}

resource "aws_route_table" "route_01" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw1.id
  }
  tags = {
    Name = var.rt_01
  }
}

resource "aws_route_table_association" "pub_route" {
  subnet_id = aws_subnet.pub_sub.id
  route_table_id = aws_route_table.route_01.id
}

resource "aws_route_table" "route_02" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat1.id
  }
  tags = {
    Name = var.rt_02
  }
}

resource "aws_route_table_association" "priv_route" {
  count = length(var.priv_sub_cidr)
  subnet_id = aws_subnet.priv_sub[count.index].id
  route_table_id = aws_route_table.route_02.id
}

resource "tls_private_key" "new_key1" {
  algorithm   = "RSA"
  rsa_bits = "2048"
}

resource "aws_key_pair" "key01" {
  key_name = "key01"
  public_key = tls_private_key.new_key1.public_key_openssh
}

#resource "local_file" "private_key" {
#  depends_on = [
#    tls_private_key.new_key,
#  ]
#  content = tls_private_key.new_key.private_key_pem
#  filename = "key.pem"
#}
#
# bastion host

resource "aws_security_group" "bastion_default_rules" {
  name = var.bastion_sg
  vpc_id = aws_vpc.vpc.id
  dynamic ingress {
    for_each = var.bastion_ports
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = [ingress.value]
      protocol    = "tcp"
    }
  }
  tags = {
    Name = var.bastion_sg
  }
}

resource "aws_instance" "bastion" {
  ami  = var.ubuntu_ami
  instance_type = "t2.micro"
  subnet_id = aws_subnet.pub_sub.id
  associate_public_ip_address = true
  key_name = "${aws_key_pair.key01.key_name}"
  vpc_security_group_ids = [aws_security_group.bastion_default_rules.id]
  tags = {
    Name = var.bastion_name
  }
}

resource "aws_security_group" "node_default_rules" {
  name = var.node_sg
  vpc_id = aws_vpc.vpc.id
  dynamic ingress {
    for_each = var.node_ports
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = [ingress.value]
      protocol    = "tcp"
    }
  }
  tags = {
    Name = var.node_sg
  }
}

resource "aws_instance" "node" {
  count = length(var.priv_sub_cidr)
  ami  = var.ubuntu_ami
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.key01.key_name}"
  subnet_id = aws_subnet.priv_sub[count.index].id
  associate_public_ip_address = false
  vpc_security_group_ids = [aws_security_group.node_default_rules.id]
  tags = {
    Name = var.node_name
  }
}


